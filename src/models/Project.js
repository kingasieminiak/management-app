import uidd from 'uuid/v1';

class Project {
  constructor(name, startDate, timeSlack) {
    this.name = name;
    this.startDate = startDate;
    this.timeSlack = parseInt(timeSlack, 10);
    this.id = uidd();
    this.tasks = [];
    this.employees = [];
  }

  assignEmployee = (employeeId) => {
    if (!this.isEmployeeIdAssigned(employeeId)) {
      return this.employees.push(employeeId);
    } else {
      throw new Error('Employee already assigned');
    }
  }

  unassignEmployee = (employeeId) => {
    if (this.isEmployeeIdAssigned(employeeId)) {
      return this.employees = this.employees.filter(employee => employee !== employeeId);
    } else {
      throw new Error('Employee already unassigned');
    }
  }

  assignTask = (task) => {
    if (!this.isTaskIdAssigned(task.id)) {
      return this.tasks.push(task);
    } else {
      throw new Error('Task already assigned');
    }
  }

  unassignTask = (taskId) => {
    if (this.isTaskIdAssigned(taskId)) {
      return this.tasks = this.tasks.filter(task => task.id !== taskId);
    } else {
      throw new Error('Task already unassigned');
    }
  }

  getTasksTime() {
    return this.tasks.reduce((acc, curr) => acc + curr.timeToComplete, 0) + this.timeSlack;
  }

  calculateEndDate() {
    const date = new Date(this.startDate);
    date.setDate(date.getDate() + this.getTasksTime());

    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  }

  isEmployeeIdAssigned(employeeId) {
    return this.employees.find(employee => employee === employeeId) !== undefined;
  }

  isTaskIdAssigned(taskId) {
    return this.tasks.find(task => task.id === taskId) !== undefined;
  }
}

export default Project;
