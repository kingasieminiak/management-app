import uidd from 'uuid/v1';

class Task {
  constructor(name, description, timeToComplete, id) {
    this.name = name;
    this.description = description;
    this.timeToComplete = parseInt(timeToComplete, 10);
    this.id = uidd();
    this.project = null;
  }

  assignToProject = (projectId) => {
    if (!this.isAssignToProject()) {
      return this.project = projectId;
    } else {
      throw new Error('Task already assigned to project');
    }
  }

  unassignFromProject = () => {
    if (this.isAssignToProject()) {
      return this.project = null;
    } else {
      throw new Error('Task not assigned to this project');
    }
  }

  isAssignToProject = () => {
    return this.project !== null;
  }
}

export default Task;
