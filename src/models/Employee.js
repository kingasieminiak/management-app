import uidd from 'uuid/v1';
class Employee {
  constructor(firstName, lastName, supervisor) {
    this.firsName = firstName;
    this.lastName = lastName;
    this.supervisor = supervisor;
    this.id = uidd();
    this.projects = [];
    this.tasks = [];
  }

  assignToProject = (projectId) => {
    if (!this.isProjectIdAssigned(projectId) && this.isEmployeeAvalible()) {
      return this.projects.push(projectId);
    } else if (!this.isEmployeeAvalible()) {
      throw new Error('Employee already assigned to 2 project');
    } else {
      throw new Error('Employee already assigned to this project');
    }
  }

  unassignFromProject = (projectId) => {
    if (this.isProjectIdAssigned(projectId)) {
      return this.projects = this.projects.filter(project => project !== projectId);
    } else {
      throw new Error('Employee already unassigned from this project');
    }
  }

  getFullName() {
    return `${this.firsName} ${this.lastName}`;
  }

  getSupervisorData() {
    return this.supervisor; // TO DO: supervisor should be employee object;
  }

  isProjectIdAssigned(projectId) {
    return this.projects.includes(projectId);
  }

  isEmployeeAvalible() {
    return this.projects.length < 2;
  }
}

export default Employee;
