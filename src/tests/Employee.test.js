import Employee from '../models/Employee';

describe('Employee Model', () => {
  let employeeModel;

  const employee = {
    firstName: 'John',
    lastName: 'Doe',
    supervisor: 'Ken Adams',
  }

  beforeEach(() => {
    employeeModel = new Employee(employee.firstName, employee.lastName, employee.supervisor);
  });

  test("New employee should be named properly", () => {
    expect(employeeModel.getFullName()).toBe('John Doe');
  });

  test("New employee should be available", () => {
    expect(employeeModel.isEmployeeAvalible()).toBe(true);
  });

  test("Assigning new project should update projects list", () => {
    employeeModel.assignToProject(1);
    expect(employeeModel.isProjectIdAssigned(1)).toBe(true);
  });

  test("Assigning two projects should make user unavailable", () => {
    employeeModel.assignToProject(1);
    employeeModel.assignToProject(2);
    expect(employeeModel.isEmployeeAvalible()).toBe(false);
  });

  test("Unassign should update projects list", () => {
    employeeModel.assignToProject(1);
    employeeModel.unassignFromProject(1);
    expect(employeeModel.isProjectIdAssigned(1)).toBe(false);
  });

  test("Unassigning from nonassigned project should throw an error", () => {
    expect(() => {
      employeeModel.unassignFromProject(1);
    }).toThrow();
  });

  test("assignToProject should throw Error when is called with duplicated projectId", () => {
    employeeModel.assignToProject(1);
    expect(() => {
      employeeModel.assignToProject(1);
    }).toThrow();
  });
});
