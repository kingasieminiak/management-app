import React, { useContext } from 'react';

import { ProjectsContext } from '../hooks/projectsManager';
import { TasksContext } from '../hooks/tasksManager';

const TaskItem = ({ task }) => {
  const { deleteTask, unassignFromProject, assignToProject } = useContext(TasksContext);
  const { projects, assignTask, unassignTask, removeTaskReferences } = useContext(ProjectsContext);

  const handleDeleteTask = (taskId) => {
    removeTaskReferences(taskId);
    deleteTask(taskId);
  }

  const handleAssignToProject = (task, projectId) => {
    if (assignToProject(task.id, projectId)) {
      assignTask(projectId, task);
    }
  }

  const handleUnAssignFromProject = (taskId, projectId) => {
    if (unassignFromProject(taskId, projectId)) {
      unassignTask(projectId, taskId);
    }
  }

  return (
    <div className="project">
      <p><strong>Name: </strong><span></span>{task.name}</p>
      <p><strong>Description: </strong><span></span>{task.description}</p>
      <p><strong>Estimated time: </strong><span></span>{task.timeToComplete}</p>

      {projects.length > 0 && (
        <div className="spacedContainer">
          <p><strong>Projects to assign</strong></p>
          <ul>{projects.map(project => (
            <li key={project.id}>
              <span>{project.name}</span>
              {project.isTaskIdAssigned(task.id) ? (
                <button onClick={() => handleUnAssignFromProject(task.id, project.id)}>unassign</button>
                ) : (
                <button onClick={() => handleAssignToProject(task, project.id)}>assign</button>
              )}
            </li>
          ))}</ul>
        </div>
      )}

      {task.project !== null && (
        <div className="spacedContainer">
          <p><strong>Assigned project</strong></p>
          <ul><li>{`Project id: ${task.project}`}</li></ul>
        </div>
      )}

      <div className="spacedContainer">
        <button onClick={() => handleDeleteTask(task.id)}>delete</button>
      </div>
  </div>
  );
}

export default TaskItem;
