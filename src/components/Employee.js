import React, { useContext } from 'react';
import { EmployeesContext } from '../hooks/employeesManager';
import { ProjectsContext } from '../hooks/projectsManager';

const EmployeeItem = ({ employee }) => {
  const { deleteEmployee } = useContext(EmployeesContext);
  const { removeEmployeeReferences } = useContext(ProjectsContext);

  const handleDeleteEmployee = (employeeId) => {
    removeEmployeeReferences(employeeId);
    deleteEmployee(employeeId);
  }

  return (
    <div className="employee">
      <p><strong>Name: </strong><span></span>{employee.getFullName()}</p>
      <p><strong>Supervisor: </strong><span></span>{employee.getSupervisorData()}</p>

      {employee.projects.length > 0 && (
        <div className="spacedContainer">
          <p><strong></strong></p>
          <ul>{employee.projects.map(project => <li key={project}>{`Project id: ${project}`}</li>)}</ul>
        </div>
      )}

      <div className="spacedContainer">
        <button onClick={() => handleDeleteEmployee(employee.id)}>delete</button>
      </div>
  </div>
  );
}

export default EmployeeItem;
