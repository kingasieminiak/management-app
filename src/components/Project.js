import React, { useContext } from 'react';

import { EmployeesContext } from '../hooks/employeesManager';
import { ProjectsContext } from '../hooks/projectsManager';
import { TasksContext } from '../hooks/tasksManager';

const ProjectItem = ({ project }) => {
  const { deleteProject, assignEmployee, unassignEmployee } = useContext(ProjectsContext);
  const { employees, assignToProject, unassignFromProject, removeProjectReference } = useContext(EmployeesContext);
  const { removeProjectReferenceFromTasks } = useContext(TasksContext);

  const handleAssignEmployee = (projectId, employeeId) => {
    if(assignToProject(employeeId, projectId)) {
      assignEmployee(projectId, employeeId);
    }
  }

  const handleUnassignEmployee = (projectId, employeeId) => {
   if( unassignFromProject(employeeId, projectId)) {
     unassignEmployee(projectId, employeeId);
   }
  }

  const handleDeleteProject = (projectId, employeeId) => {
    removeProjectReference(projectId);
    removeProjectReferenceFromTasks(projectId);
    deleteProject(projectId)
  }

  const generateEmployessList = (employee, project) => (
    <li key={employee.id}>
      {employee.getFullName()}

      {project.isEmployeeIdAssigned(employee.id) ? (
        <button onClick={() => handleUnassignEmployee(project.id, employee.id)}>unassign</button>) : (
        <button onClick={() => handleAssignEmployee(project.id, employee.id)}>assign</button>
      )}
    </li>
  );

  return (
    <div className="project">
      <div className="spacedContainer">
        <p><strong>Name: </strong><span></span>{project.name}</p>
        <p><strong>Starting date: </strong><span></span>{project.startDate}</p>
        <p><strong>Ending date: </strong><span></span>{project.calculateEndDate()}</p>
        <p><strong>Time slack: </strong><span></span>{project.timeSlack}</p>
        <p><strong>Time sum: </strong><span></span>{project.getTasksTime()}</p>
      </div>

      <div className="spacedContainer">
        <p><strong>{employees.length ? 'Assign employee' : 'No employees to assign'}</strong></p>
        {employees.length > 0 && <ul>{employees.map(employee => generateEmployessList(employee, project))}</ul>}
      </div>

      <div className="spacedContainer">
        <p><strong>Assigned employees</strong></p>
        {project.employees.length > 0 ? <ul>{project.employees.map(employee => <li key={employee}>{`Employee id: ${employee}`}</li>)}</ul> : <p>No employees assigned</p>}
      </div>

      <div className="spacedContainer">
        <p><strong>Assigned tasks</strong></p>
        {project.tasks.length > 0 ? <ul>{project.tasks.map(task => <li key={task.id}>{`Task: ${task.name}`}</li>)}</ul> : <p>No tasks assigned</p>}
      </div>

      <div className="spacedContainer">
        <button onClick={() => handleDeleteProject(project.id)}>delete</button>
      </div>
    </div>
  );
}

export default ProjectItem;
