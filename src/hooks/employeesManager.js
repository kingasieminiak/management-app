import React, { useState, createContext } from 'react';
import Employee from '../models/Employee';

export const EmployeesContext = createContext({});

function EmployeeManager({ children }) {
  const [ employees, setEmployee ] = useState([]);

  const addEmployee = (employee) => {
    const newEmployee = new Employee(
      employee.firstName || 'Anonymous',
      employee.lastName || 'Anonymous',
      employee.supervisor || 'No supervisor',
     );

    const employeesList = [ ...employees, newEmployee ];
    setEmployee(employeesList);
  }

  const deleteEmployee = (employeeId) => {
    const employeesList = employees.filter(employee => employee.id !== employeeId);
    setEmployee(employeesList);
  }

  const assignToProject = (employeeId, projectId) => {
    let employee = findEmployeeById(employeeId);

    try {
      employee.assignToProject(projectId);
    } catch(error) {
      alert(error);
      return false;
    } finally {
      setEmployee(employees);
    }

    return true;
  }

  const unassignFromProject = (employeeId, projectId) => {
    let employee = findEmployeeById(employeeId);

    try {
      employee.unassignFromProject(projectId);
    } catch(error) {
      alert(error);
      return false;
    } finally {
      setEmployee(employees);
    }

    return true;
  }

  const removeProjectReference = (projectId) => {
    employees.map(employee => {
      employee.projects.map(project => {
        if(project === projectId) {
          try {
            employee.unassignFromProject(projectId);
          } catch(error) {
            alert(error);
            return false;
          } finally {
            setEmployee(employees);
          }

          return true;
        }
      })
    });
  }

  const findEmployeeById = (employeeId) => {
    return employees.find(employee => employee.id === employeeId);
  }

  return (
    <EmployeesContext.Provider value={{
      employees,
      addEmployee,
      deleteEmployee,
      assignToProject,
      unassignFromProject,
      removeProjectReference
    }}>
      {children}
    </EmployeesContext.Provider>
  );
}

export default EmployeeManager;
