import React, { useState, createContext } from 'react';
import Task from '../models/Task';

export const TasksContext = createContext({});

function TasksManager({ children }) {
  const [ tasks, setTasks ] = useState([]);

  const addTask = (task) => {
    const newTask = new Task(
      task.name || 'No task name',
      task.description || 'No description',
      task.timeToComplete || 'No estimated time'
    );

    const tasksList = [ ...tasks, newTask ];
    setTasks(tasksList);
  }

  const deleteTask = (taskId) => {
    const tasksList = tasks.filter(task => task.id !== taskId);
    setTasks(tasksList);
  }

  const assignToProject = (taskId, projectId) => {
    let task = findTaskById(taskId);

    try {
      task.assignToProject(projectId);
    } catch(error) {
      alert(error)
      return false;
    } finally {
      setTasks(tasks);
    }

    return true;
  }

  const unassignFromProject = (taskId, projectId) => {
    let task = findTaskById(taskId);

    try {
      task.unassignFromProject(projectId);
    } catch(error) {
      alert(error)
      return false
    } finally {
      setTasks(tasks);
    }

    return true;
  }

  const removeProjectReferenceFromTasks = (projectId) => {
    tasks.map(task => {
      if(task.project === projectId) {
        try {
          task.unassignFromProject(projectId);
        } catch(error) {
          alert(error)
          return false
        } finally {
          setTasks(tasks);
        }

        return true;
      }
    });
  };

  const findTaskById = (taskId) => {
    return tasks.find(task => task.id === taskId);
  }

  return (
    <TasksContext.Provider value={{
      tasks,
      addTask,
      deleteTask,
      assignToProject,
      unassignFromProject,
      removeProjectReferenceFromTasks
    }}>
      {children}
    </TasksContext.Provider>
  );
}

export default TasksManager;
