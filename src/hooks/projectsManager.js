import React, { useState, createContext } from 'react';
import Project from '../models/Project';

export const ProjectsContext = createContext({});

function ProjectsManager({ children }) {
  const [ projects, setProjects ] = useState([]);

  const addProject = (project) => {
    const newProject = new Project(
      project.name || 'No project name',
      project.startDate || 'No date added',
      project.timeSlack || 'No time slack'
    );

    const projectsList = [ ...projects, newProject ];
    setProjects(projectsList);
  }

  const deleteProject = (projectId) => {
    const projectsList = projects.filter(project => project.id !== projectId);
    setProjects(projectsList);
  }

  const assignEmployee = (projectId, employeeId) => {
    let project = getProjectById(projectId);

    try {
      project.assignEmployee(employeeId);
    } catch(error) {
      alert(error);
      return false;
    } finally {
      setProjects(projects);
    }

    return true;
  }

  const unassignEmployee = (projectId, employeeId) => {
    let project = getProjectById(projectId);

    try {
      project.unassignEmployee(employeeId);
    } catch(error) {
      alert(error);
      return true;
    } finally {
      setProjects(projects);
    }

    return true;
  }

  const assignTask = (projectId, task) => {
    let project = getProjectById(projectId);

    try {
      project.assignTask(task);
    } catch(error) {
      alert(error);
      return false
    } finally {
      setProjects(projects);
    }

    return true;
  }

  const unassignTask = (projectId, taskId) => {
    let project = getProjectById(projectId);

    try {
      project.unassignTask(taskId);
    } catch(error) {
      alert(error);
      return false;
    } finally {
      setProjects(projects);
    }

    return true;
  }

  const removeEmployeeReferences = (employeeId) => {
    projects.map(project => {
      project.employees.map(employee => {
        if(employee === employeeId) {
          try {
            return project.unassignEmployee(employeeId);
          } catch(error) {
            alert(error);
            return false;
          } finally {
            setProjects(projects);
          }
        }

        return true;
      })
    });
  }

  const removeTaskReferences = (taskId) => {
    projects.map(project => {
      project.tasks.map(task => {
        if(task.id === taskId) {
          try {
            project.unassignTask(taskId);
          } catch(error) {
            alert(error);
            return false;
          } finally {
            setProjects(projects);
          }

          return true;
        }
      })
    });
  }

  const getProjectById = (projectId) => {
    return projects.find(project => project.id === projectId);
  }

  const getTotalProjectsDays = () => {
    return projects.map(project => project.getTasksTime()).reduce((acc, curr) => acc + curr, 0);
  }

  return (
    <ProjectsContext.Provider value={{
      projects,
      addProject,
      deleteProject,
      assignEmployee,
      unassignEmployee,
      assignTask,
      unassignTask,
      removeEmployeeReferences,
      removeTaskReferences,
      getTotalProjectsDays
    }}>
      {children}
    </ProjectsContext.Provider>
  );
}

export default ProjectsManager;
