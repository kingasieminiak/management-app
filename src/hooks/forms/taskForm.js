import React, { useContext } from 'react';
import useInput from './useInput';
import { TasksContext } from '../tasksManager';

function TaskForm() {
  const { addTask } = useContext(TasksContext);

  const name = useInput('');
  const description = useInput('');
  const timeToComplete = useInput('');

  const submit = (e) => {
    e.preventDefault();

    let task = { name: name.value, description: description.value, timeToComplete: timeToComplete.value };
    addTask(task);
  };

  return (
    <form onSubmit={submit}>
      <input placeholder="Task name" type="text" {...name} required />
      <input placeholder="Task description" type="text" {...description} required />
      <input placeholder="Estimated time" type="number" {...timeToComplete} required />
      <button type="submit">Submit new task</button>
    </form>
  );
}

export default TaskForm;
