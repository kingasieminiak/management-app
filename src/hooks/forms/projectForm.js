import React, { useContext } from 'react';
import useInput from './useInput';
import { ProjectsContext } from '../projectsManager';

function ProjectFrom() {
  const { addProject } = useContext(ProjectsContext);

  const name = useInput('');
  const startDate = useInput('');
  const timeSlack = useInput('');

  const submit = (e) => {
    e.preventDefault();

    let project = { name: name.value, startDate: startDate.value, timeSlack: timeSlack.value };
    addProject(project);
  };

  return (
    <form onSubmit={submit}>
      <input placeholder="Project name" type="text" {...name} required />
      <input placeholder="Start Date" type="date" {...startDate} required />
      <input placeholder="Time slck" type="number" {...timeSlack} required />
      <button type="submit">Submit new project</button>
    </form>
  );
}

export default ProjectFrom;
