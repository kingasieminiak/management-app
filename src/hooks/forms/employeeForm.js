import React, { useContext } from 'react';
import useInput from './useInput';
import { EmployeesContext } from '../employeesManager';

function EmployeeForm() {
  const { addEmployee } = useContext(EmployeesContext);

  const firstName = useInput('');
  const lastName = useInput('');
  const supervisor = useInput('');

  const submit = (e) => {
    e.preventDefault();

    let employee = { firstName: firstName.value, lastName: lastName.value, supervisor: supervisor.value };
    addEmployee(employee);
  };

  return (
    <form onSubmit={submit}>
      <input placeholder="First name" type="text" {...firstName} required />
      <input placeholder="Last name" type="text" {...lastName} required />
      <input placeholder="Supervisor" type="text" {...supervisor} required />
      <button type="submit">Submit new employee</button>
    </form>
  );
}

export default EmployeeForm;
