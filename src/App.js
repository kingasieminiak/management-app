import React, { Fragment, useContext } from 'react';

import ProjectsManager, { ProjectsContext } from './hooks/projectsManager';
import EmployeeManager, { EmployeesContext } from './hooks/employeesManager';
import TasksManager, { TasksContext } from './hooks/tasksManager';

import ProjectFrom from './hooks/forms/projectForm';
import EmployeeForm from './hooks/forms/employeeForm';
import TaskForm from './hooks/forms/taskForm';

import ProjectItem from './components/Project';
import EmployeeItem from './components/Employee';
import TaskItem from './components/Task';

const ProjectsList = () => {
  const { projects, getTotalProjectsDays } = useContext(ProjectsContext);
  return (
    <Fragment>
      <h4>{`Total projects days: ${getTotalProjectsDays()}`}</h4>
      <div>{projects.length ? projects.map(project => <ProjectItem key={project.id} project={project} />) : "No project added"}</div>
    </Fragment>
  );
}

function EmployeesList() {
  const { employees } = useContext(EmployeesContext);

  return <div>{employees.length ? employees.map(employee => <EmployeeItem key={employee.id} employee={employee} /> ) : "No employees added"}</div>;
}

function TasksList() {
  const { tasks } = useContext(TasksContext);
  return <div>{tasks.length ? tasks.map(task => <TaskItem key={task.id} task={task} />) : "No tasks added"}</div>
}

function App() {
  return (
    <div className="app">
      <ProjectsManager>
        <EmployeeManager>
          <TasksManager>
            <div className="app__column">
              <h3>Add new project</h3>
              <ProjectFrom />

              <h3>Project list</h3>
              <ProjectsList />
            </div>

            <div className="app__column">
              <h3>Add new employee</h3>
              <EmployeeForm />

              <h3>Employees list</h3>
              <EmployeesList />
            </div>

            <div className="app__column">
              <h3>Add new task</h3>
              <TaskForm />

              <h3>Tasks list</h3>
              <TasksList />
            </div>
          </TasksManager>
        </EmployeeManager>
      </ProjectsManager>
    </div>
  );
}


export default App;
